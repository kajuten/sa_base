var express = require('express');
var router = express.Router();

var restaurants = require('restaurants');
var stops = require('stops');

router.post('/nearby', function(req, res) {
    if (req.body.stops.map !== undefined) {
        res.render('error', {message: 'You can only check 1 stop'});
    } else if (req.body.distance === undefined) {
        res.render('error', {message: 'You need to enter a distance'});
    } else {
        var stop = JSON.parse(req.body.stops);
        console.log(stop);
        stop = new stops.Stop(stop.id, stop.lon, stop.lat, stop.name);
        console.log(stop);
        restaurants.getNearbyRestaurants(stop, req.body.distance, function(rows) {
            res.render('restaurants', { stop: stop, restaurants: rows });
        })
    }
});

module.exports = router;
