var express = require('express');
var router = express.Router();

var stops = require('stops');
var route = require('route');
var _ = require('underscore');

router.post('/', function(req, res) {
    if (req.body.start !== undefined && req.body.stop !== undefined) {
        route.getRoute(req.body.start, req.body.stop, function(rows) {
            route.getRouteNames(rows, function(r) {
                res.send(r);
            });
        });
    } else {
        res.send('false');
    }
});

router.post('/new', function(req, res) {
    if (req.body.name !== undefined && req.body.stops !== undefined
        && req.body.stops.length !== undefined && req.body.stops.length >= 2) {
        req.body.stops = req.body.stops.map(JSON.parse);
        var name = req.body.name + ': ' + req.body.stops[0].name + ' => ' + req.body.stops[req.body.stops.length - 1].name;
        
        route.addNewRoute(name, req.body.stops);
        res.render('route', { name: name, stops: req.body.stops });
    } else {
        res.render('error', { message: 'You must enter a name and check at least 2 stops' });
    }
});

router.post('/check', function(req, res) {
    if (req.body.stops.length !== 2) {
        res.render('error', {message: 'You can only check 2 stops'});
    } else {
        req.body.stops = req.body.stops.map(JSON.parse);
        
        route.getRoute(req.body.stops[0].name, req.body.stops[1].name, function(rows) {
            route.getRouteNames(rows, function(r) {
                var distinct = [];
                r.forEach(function(e) {
                    if (!_.find(distinct, function(d) { return d.name.split(':')[0] === e.name.split(':')[0] })) {
                        distinct[distinct.length] = e;
                    }
                });
                
                res.render('check', { start: req.body.stops[0].name, stop: req.body.stops[1].name, stops: distinct });
            });
        });
    }
});

module.exports = router;
