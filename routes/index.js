var express = require('express');
var router = express.Router();

var stops = require('stops');
var Filter = require('filter');

function push(filters, filter) {
  if (filter.value !== null && filter.value !== undefined && filter.value !== '') {
    filters.push(filter);
  }
}

router.post('/filter', function(req, res) {
  
  var filter = new Filter.Filter();
  console.log(req.body);
  
  stops.getAllStops(function(rows) {
    var filters = [];
    push(filters, new Filter.NameFilter(req.body.name));
    push(filters, new Filter.LatFilter(req.body.lat1));
    push(filters, new Filter.LatFilter(req.body.lat2));
    push(filters, new Filter.LonFilter(req.body.lon1));
    push(filters, new Filter.LonFilter(req.body.lon2));
    
    rows = filter.each(rows, filters);
    res.render('index', {stops: rows });
  });
});

router.get('/', function(req, res) {
  stops.getAllStops(function(rows) {
    res.render('index', { stops: rows });
  });
});

module.exports = router;
