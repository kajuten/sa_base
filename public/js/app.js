/// <reference path="../../typings/angularjs/angular.d.ts"/>

var app = angular.module('stopsBrowser', ['ngMaterial']);

function setAction(id, action) {
    var form = document.getElementById(id);
    form.action = action;
    return true;
}

function check() {
    return setAction('stops', '/route/check');
}

function newRoute() {
    return setAction('stops', '/route/new');
}

function nearby() {
    return setAction('stops', '/restaurants/nearby');
}